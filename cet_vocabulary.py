import re

import pandas as pd
import pdfplumber


def word_prase(word, level):
    if '/' in word:
        w1, w2 = word.split('/')
        if w2.startswith('Ｇ'):
            w2_len = len(w2) - 1
            w2 = w1[:-w2_len] + w2[1:]
            print(w1, w2)
        else:
            w1, w2 = word.split('/')
        return [[w1, level], [w2, level]]
    else:
        return [[word, level]]


def line_prase(line, level=4):
    line_words = []
    for w in line.strip().split():
        line_words.extend(word_prase(w, level))
    return line_words


def page_prase(page):
    vocabulary = []
    text = page.extract_text()
    text = re.sub(r'★\n', '★', text)
    text = re.sub(r'\n.*?$', '', text)
    page_no = int(page.extract_words()[-1]['text'])
    if page_no % 2 == 1:
        text = re.sub(r' 词\n表$', '', text)
    else:
        text = text.replace('全国大学英语四、六级考试大纲(２０１６年修订版)\n', '')
    for line in text.split('\n'):
        if line.startswith('★'):
            vocabulary.extend(line_prase(line.replace('★', ''), 6))
        else:
            vocabulary.extend(line_prase(line,4))
    return vocabulary


def main(input_file, out_file):
    pdf_reader = pdfplumber.open(input_file)
    full_vocabulary = []
    for page_index in range(20, 148):
        page = pdf_reader.pages[page_index]
        vocabulary = page_prase(page)
        full_vocabulary.extend(vocabulary)
    full_vocabulary_df = pd.DataFrame(full_vocabulary, columns=['word', 'cet_level'])
    full_vocabulary_df.to_excel(out_file)


# vocabulary = page_prase(page_26)

input_file = 'cet_outline.pdf'
main('cet_outline.pdf', 'cet-vocabulary.xlsx')
